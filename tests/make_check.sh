#!/bin/bash

set -x

check_return_value () {
    if [ $1 != 0 ] ; then
        exit $1
    fi
}

cd $1

VERSION=`rpmspec -q --srpm --qf "%{version}" ibus-libzhuyin.spec 2>/dev/null`

if test -d ibus-libzhuyin-$VERSION-build;
then cd ibus-libzhuyin-$VERSION-build;
fi

cd ibus-libzhuyin-$VERSION

./configure --prefix=/usr
check_return_value $?
make check
exit $?
